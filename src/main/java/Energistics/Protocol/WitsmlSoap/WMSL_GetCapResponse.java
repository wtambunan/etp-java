/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package Energistics.Protocol.WitsmlSoap;

import org.apache.avro.specific.SpecificData;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class WMSL_GetCapResponse extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -7543546862187732761L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"WMSL_GetCapResponse\",\"namespace\":\"Energistics.Protocol.WitsmlSoap\",\"fields\":[{\"name\":\"Result\",\"type\":\"int\"},{\"name\":\"CapabilitiesOut\",\"type\":\"string\"},{\"name\":\"SuppMsgOut\",\"type\":\"string\"}],\"messageType\":\"8\",\"protocol\":\"8\",\"senderRole\":\"store\",\"protocolRoles\":\"store,customer\",\"fullName\":\"Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse\",\"depends\":[]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  @Deprecated public int Result;
  @Deprecated public java.lang.CharSequence CapabilitiesOut;
  @Deprecated public java.lang.CharSequence SuppMsgOut;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public WMSL_GetCapResponse() {}

  /**
   * All-args constructor.
   * @param Result The new value for Result
   * @param CapabilitiesOut The new value for CapabilitiesOut
   * @param SuppMsgOut The new value for SuppMsgOut
   */
  public WMSL_GetCapResponse(java.lang.Integer Result, java.lang.CharSequence CapabilitiesOut, java.lang.CharSequence SuppMsgOut) {
    this.Result = Result;
    this.CapabilitiesOut = CapabilitiesOut;
    this.SuppMsgOut = SuppMsgOut;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return Result;
    case 1: return CapabilitiesOut;
    case 2: return SuppMsgOut;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: Result = (java.lang.Integer)value$; break;
    case 1: CapabilitiesOut = (java.lang.CharSequence)value$; break;
    case 2: SuppMsgOut = (java.lang.CharSequence)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'Result' field.
   * @return The value of the 'Result' field.
   */
  public java.lang.Integer getResult() {
    return Result;
  }

  /**
   * Sets the value of the 'Result' field.
   * @param value the value to set.
   */
  public void setResult(java.lang.Integer value) {
    this.Result = value;
  }

  /**
   * Gets the value of the 'CapabilitiesOut' field.
   * @return The value of the 'CapabilitiesOut' field.
   */
  public java.lang.CharSequence getCapabilitiesOut() {
    return CapabilitiesOut;
  }

  /**
   * Sets the value of the 'CapabilitiesOut' field.
   * @param value the value to set.
   */
  public void setCapabilitiesOut(java.lang.CharSequence value) {
    this.CapabilitiesOut = value;
  }

  /**
   * Gets the value of the 'SuppMsgOut' field.
   * @return The value of the 'SuppMsgOut' field.
   */
  public java.lang.CharSequence getSuppMsgOut() {
    return SuppMsgOut;
  }

  /**
   * Sets the value of the 'SuppMsgOut' field.
   * @param value the value to set.
   */
  public void setSuppMsgOut(java.lang.CharSequence value) {
    this.SuppMsgOut = value;
  }

  /**
   * Creates a new WMSL_GetCapResponse RecordBuilder.
   * @return A new WMSL_GetCapResponse RecordBuilder
   */
  public static Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder newBuilder() {
    return new Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder();
  }

  /**
   * Creates a new WMSL_GetCapResponse RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new WMSL_GetCapResponse RecordBuilder
   */
  public static Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder newBuilder(Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder other) {
    return new Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder(other);
  }

  /**
   * Creates a new WMSL_GetCapResponse RecordBuilder by copying an existing WMSL_GetCapResponse instance.
   * @param other The existing instance to copy.
   * @return A new WMSL_GetCapResponse RecordBuilder
   */
  public static Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder newBuilder(Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse other) {
    return new Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder(other);
  }

  /**
   * RecordBuilder for WMSL_GetCapResponse instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<WMSL_GetCapResponse>
    implements org.apache.avro.data.RecordBuilder<WMSL_GetCapResponse> {

    private int Result;
    private java.lang.CharSequence CapabilitiesOut;
    private java.lang.CharSequence SuppMsgOut;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.Result)) {
        this.Result = data().deepCopy(fields()[0].schema(), other.Result);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.CapabilitiesOut)) {
        this.CapabilitiesOut = data().deepCopy(fields()[1].schema(), other.CapabilitiesOut);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.SuppMsgOut)) {
        this.SuppMsgOut = data().deepCopy(fields()[2].schema(), other.SuppMsgOut);
        fieldSetFlags()[2] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing WMSL_GetCapResponse instance
     * @param other The existing instance to copy.
     */
    private Builder(Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.Result)) {
        this.Result = data().deepCopy(fields()[0].schema(), other.Result);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.CapabilitiesOut)) {
        this.CapabilitiesOut = data().deepCopy(fields()[1].schema(), other.CapabilitiesOut);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.SuppMsgOut)) {
        this.SuppMsgOut = data().deepCopy(fields()[2].schema(), other.SuppMsgOut);
        fieldSetFlags()[2] = true;
      }
    }

    /**
      * Gets the value of the 'Result' field.
      * @return The value.
      */
    public java.lang.Integer getResult() {
      return Result;
    }

    /**
      * Sets the value of the 'Result' field.
      * @param value The value of 'Result'.
      * @return This builder.
      */
    public Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder setResult(int value) {
      validate(fields()[0], value);
      this.Result = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'Result' field has been set.
      * @return True if the 'Result' field has been set, false otherwise.
      */
    public boolean hasResult() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'Result' field.
      * @return This builder.
      */
    public Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder clearResult() {
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'CapabilitiesOut' field.
      * @return The value.
      */
    public java.lang.CharSequence getCapabilitiesOut() {
      return CapabilitiesOut;
    }

    /**
      * Sets the value of the 'CapabilitiesOut' field.
      * @param value The value of 'CapabilitiesOut'.
      * @return This builder.
      */
    public Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder setCapabilitiesOut(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.CapabilitiesOut = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'CapabilitiesOut' field has been set.
      * @return True if the 'CapabilitiesOut' field has been set, false otherwise.
      */
    public boolean hasCapabilitiesOut() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'CapabilitiesOut' field.
      * @return This builder.
      */
    public Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder clearCapabilitiesOut() {
      CapabilitiesOut = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'SuppMsgOut' field.
      * @return The value.
      */
    public java.lang.CharSequence getSuppMsgOut() {
      return SuppMsgOut;
    }

    /**
      * Sets the value of the 'SuppMsgOut' field.
      * @param value The value of 'SuppMsgOut'.
      * @return This builder.
      */
    public Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder setSuppMsgOut(java.lang.CharSequence value) {
      validate(fields()[2], value);
      this.SuppMsgOut = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'SuppMsgOut' field has been set.
      * @return True if the 'SuppMsgOut' field has been set, false otherwise.
      */
    public boolean hasSuppMsgOut() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'SuppMsgOut' field.
      * @return This builder.
      */
    public Energistics.Protocol.WitsmlSoap.WMSL_GetCapResponse.Builder clearSuppMsgOut() {
      SuppMsgOut = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    @Override
    public WMSL_GetCapResponse build() {
      try {
        WMSL_GetCapResponse record = new WMSL_GetCapResponse();
        record.Result = fieldSetFlags()[0] ? this.Result : (java.lang.Integer) defaultValue(fields()[0]);
        record.CapabilitiesOut = fieldSetFlags()[1] ? this.CapabilitiesOut : (java.lang.CharSequence) defaultValue(fields()[1]);
        record.SuppMsgOut = fieldSetFlags()[2] ? this.SuppMsgOut : (java.lang.CharSequence) defaultValue(fields()[2]);
        return record;
      } catch (Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  private static final org.apache.avro.io.DatumWriter
    WRITER$ = new org.apache.avro.specific.SpecificDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  private static final org.apache.avro.io.DatumReader
    READER$ = new org.apache.avro.specific.SpecificDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
