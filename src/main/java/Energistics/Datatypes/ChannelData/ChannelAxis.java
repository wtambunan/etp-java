/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package Energistics.Datatypes.ChannelData;

import org.apache.avro.specific.SpecificData;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class ChannelAxis extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 3109610657023531626L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"ChannelAxis\",\"namespace\":\"Energistics.Datatypes.ChannelData\",\"fields\":[{\"name\":\"axisName\",\"type\":\"string\"},{\"name\":\"axisPropertyKind\",\"type\":\"string\"},{\"name\":\"axisStart\",\"type\":\"double\"},{\"name\":\"axisSpacing\",\"type\":\"double\"},{\"name\":\"axisCount\",\"type\":\"int\"},{\"name\":\"axisUom\",\"type\":\"string\"}],\"fullName\":\"Energistics.Datatypes.ChannelData.ChannelAxis\",\"depends\":[]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  @Deprecated public java.lang.CharSequence axisName;
  @Deprecated public java.lang.CharSequence axisPropertyKind;
  @Deprecated public double axisStart;
  @Deprecated public double axisSpacing;
  @Deprecated public int axisCount;
  @Deprecated public java.lang.CharSequence axisUom;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public ChannelAxis() {}

  /**
   * All-args constructor.
   * @param axisName The new value for axisName
   * @param axisPropertyKind The new value for axisPropertyKind
   * @param axisStart The new value for axisStart
   * @param axisSpacing The new value for axisSpacing
   * @param axisCount The new value for axisCount
   * @param axisUom The new value for axisUom
   */
  public ChannelAxis(java.lang.CharSequence axisName, java.lang.CharSequence axisPropertyKind, java.lang.Double axisStart, java.lang.Double axisSpacing, java.lang.Integer axisCount, java.lang.CharSequence axisUom) {
    this.axisName = axisName;
    this.axisPropertyKind = axisPropertyKind;
    this.axisStart = axisStart;
    this.axisSpacing = axisSpacing;
    this.axisCount = axisCount;
    this.axisUom = axisUom;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return axisName;
    case 1: return axisPropertyKind;
    case 2: return axisStart;
    case 3: return axisSpacing;
    case 4: return axisCount;
    case 5: return axisUom;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: axisName = (java.lang.CharSequence)value$; break;
    case 1: axisPropertyKind = (java.lang.CharSequence)value$; break;
    case 2: axisStart = (java.lang.Double)value$; break;
    case 3: axisSpacing = (java.lang.Double)value$; break;
    case 4: axisCount = (java.lang.Integer)value$; break;
    case 5: axisUom = (java.lang.CharSequence)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'axisName' field.
   * @return The value of the 'axisName' field.
   */
  public java.lang.CharSequence getAxisName() {
    return axisName;
  }

  /**
   * Sets the value of the 'axisName' field.
   * @param value the value to set.
   */
  public void setAxisName(java.lang.CharSequence value) {
    this.axisName = value;
  }

  /**
   * Gets the value of the 'axisPropertyKind' field.
   * @return The value of the 'axisPropertyKind' field.
   */
  public java.lang.CharSequence getAxisPropertyKind() {
    return axisPropertyKind;
  }

  /**
   * Sets the value of the 'axisPropertyKind' field.
   * @param value the value to set.
   */
  public void setAxisPropertyKind(java.lang.CharSequence value) {
    this.axisPropertyKind = value;
  }

  /**
   * Gets the value of the 'axisStart' field.
   * @return The value of the 'axisStart' field.
   */
  public java.lang.Double getAxisStart() {
    return axisStart;
  }

  /**
   * Sets the value of the 'axisStart' field.
   * @param value the value to set.
   */
  public void setAxisStart(java.lang.Double value) {
    this.axisStart = value;
  }

  /**
   * Gets the value of the 'axisSpacing' field.
   * @return The value of the 'axisSpacing' field.
   */
  public java.lang.Double getAxisSpacing() {
    return axisSpacing;
  }

  /**
   * Sets the value of the 'axisSpacing' field.
   * @param value the value to set.
   */
  public void setAxisSpacing(java.lang.Double value) {
    this.axisSpacing = value;
  }

  /**
   * Gets the value of the 'axisCount' field.
   * @return The value of the 'axisCount' field.
   */
  public java.lang.Integer getAxisCount() {
    return axisCount;
  }

  /**
   * Sets the value of the 'axisCount' field.
   * @param value the value to set.
   */
  public void setAxisCount(java.lang.Integer value) {
    this.axisCount = value;
  }

  /**
   * Gets the value of the 'axisUom' field.
   * @return The value of the 'axisUom' field.
   */
  public java.lang.CharSequence getAxisUom() {
    return axisUom;
  }

  /**
   * Sets the value of the 'axisUom' field.
   * @param value the value to set.
   */
  public void setAxisUom(java.lang.CharSequence value) {
    this.axisUom = value;
  }

  /**
   * Creates a new ChannelAxis RecordBuilder.
   * @return A new ChannelAxis RecordBuilder
   */
  public static Energistics.Datatypes.ChannelData.ChannelAxis.Builder newBuilder() {
    return new Energistics.Datatypes.ChannelData.ChannelAxis.Builder();
  }

  /**
   * Creates a new ChannelAxis RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new ChannelAxis RecordBuilder
   */
  public static Energistics.Datatypes.ChannelData.ChannelAxis.Builder newBuilder(Energistics.Datatypes.ChannelData.ChannelAxis.Builder other) {
    return new Energistics.Datatypes.ChannelData.ChannelAxis.Builder(other);
  }

  /**
   * Creates a new ChannelAxis RecordBuilder by copying an existing ChannelAxis instance.
   * @param other The existing instance to copy.
   * @return A new ChannelAxis RecordBuilder
   */
  public static Energistics.Datatypes.ChannelData.ChannelAxis.Builder newBuilder(Energistics.Datatypes.ChannelData.ChannelAxis other) {
    return new Energistics.Datatypes.ChannelData.ChannelAxis.Builder(other);
  }

  /**
   * RecordBuilder for ChannelAxis instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<ChannelAxis>
    implements org.apache.avro.data.RecordBuilder<ChannelAxis> {

    private java.lang.CharSequence axisName;
    private java.lang.CharSequence axisPropertyKind;
    private double axisStart;
    private double axisSpacing;
    private int axisCount;
    private java.lang.CharSequence axisUom;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(Energistics.Datatypes.ChannelData.ChannelAxis.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.axisName)) {
        this.axisName = data().deepCopy(fields()[0].schema(), other.axisName);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.axisPropertyKind)) {
        this.axisPropertyKind = data().deepCopy(fields()[1].schema(), other.axisPropertyKind);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.axisStart)) {
        this.axisStart = data().deepCopy(fields()[2].schema(), other.axisStart);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.axisSpacing)) {
        this.axisSpacing = data().deepCopy(fields()[3].schema(), other.axisSpacing);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.axisCount)) {
        this.axisCount = data().deepCopy(fields()[4].schema(), other.axisCount);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.axisUom)) {
        this.axisUom = data().deepCopy(fields()[5].schema(), other.axisUom);
        fieldSetFlags()[5] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing ChannelAxis instance
     * @param other The existing instance to copy.
     */
    private Builder(Energistics.Datatypes.ChannelData.ChannelAxis other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.axisName)) {
        this.axisName = data().deepCopy(fields()[0].schema(), other.axisName);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.axisPropertyKind)) {
        this.axisPropertyKind = data().deepCopy(fields()[1].schema(), other.axisPropertyKind);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.axisStart)) {
        this.axisStart = data().deepCopy(fields()[2].schema(), other.axisStart);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.axisSpacing)) {
        this.axisSpacing = data().deepCopy(fields()[3].schema(), other.axisSpacing);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.axisCount)) {
        this.axisCount = data().deepCopy(fields()[4].schema(), other.axisCount);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.axisUom)) {
        this.axisUom = data().deepCopy(fields()[5].schema(), other.axisUom);
        fieldSetFlags()[5] = true;
      }
    }

    /**
      * Gets the value of the 'axisName' field.
      * @return The value.
      */
    public java.lang.CharSequence getAxisName() {
      return axisName;
    }

    /**
      * Sets the value of the 'axisName' field.
      * @param value The value of 'axisName'.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder setAxisName(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.axisName = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'axisName' field has been set.
      * @return True if the 'axisName' field has been set, false otherwise.
      */
    public boolean hasAxisName() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'axisName' field.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder clearAxisName() {
      axisName = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'axisPropertyKind' field.
      * @return The value.
      */
    public java.lang.CharSequence getAxisPropertyKind() {
      return axisPropertyKind;
    }

    /**
      * Sets the value of the 'axisPropertyKind' field.
      * @param value The value of 'axisPropertyKind'.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder setAxisPropertyKind(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.axisPropertyKind = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'axisPropertyKind' field has been set.
      * @return True if the 'axisPropertyKind' field has been set, false otherwise.
      */
    public boolean hasAxisPropertyKind() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'axisPropertyKind' field.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder clearAxisPropertyKind() {
      axisPropertyKind = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'axisStart' field.
      * @return The value.
      */
    public java.lang.Double getAxisStart() {
      return axisStart;
    }

    /**
      * Sets the value of the 'axisStart' field.
      * @param value The value of 'axisStart'.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder setAxisStart(double value) {
      validate(fields()[2], value);
      this.axisStart = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'axisStart' field has been set.
      * @return True if the 'axisStart' field has been set, false otherwise.
      */
    public boolean hasAxisStart() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'axisStart' field.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder clearAxisStart() {
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'axisSpacing' field.
      * @return The value.
      */
    public java.lang.Double getAxisSpacing() {
      return axisSpacing;
    }

    /**
      * Sets the value of the 'axisSpacing' field.
      * @param value The value of 'axisSpacing'.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder setAxisSpacing(double value) {
      validate(fields()[3], value);
      this.axisSpacing = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'axisSpacing' field has been set.
      * @return True if the 'axisSpacing' field has been set, false otherwise.
      */
    public boolean hasAxisSpacing() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'axisSpacing' field.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder clearAxisSpacing() {
      fieldSetFlags()[3] = false;
      return this;
    }

    /**
      * Gets the value of the 'axisCount' field.
      * @return The value.
      */
    public java.lang.Integer getAxisCount() {
      return axisCount;
    }

    /**
      * Sets the value of the 'axisCount' field.
      * @param value The value of 'axisCount'.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder setAxisCount(int value) {
      validate(fields()[4], value);
      this.axisCount = value;
      fieldSetFlags()[4] = true;
      return this;
    }

    /**
      * Checks whether the 'axisCount' field has been set.
      * @return True if the 'axisCount' field has been set, false otherwise.
      */
    public boolean hasAxisCount() {
      return fieldSetFlags()[4];
    }


    /**
      * Clears the value of the 'axisCount' field.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder clearAxisCount() {
      fieldSetFlags()[4] = false;
      return this;
    }

    /**
      * Gets the value of the 'axisUom' field.
      * @return The value.
      */
    public java.lang.CharSequence getAxisUom() {
      return axisUom;
    }

    /**
      * Sets the value of the 'axisUom' field.
      * @param value The value of 'axisUom'.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder setAxisUom(java.lang.CharSequence value) {
      validate(fields()[5], value);
      this.axisUom = value;
      fieldSetFlags()[5] = true;
      return this;
    }

    /**
      * Checks whether the 'axisUom' field has been set.
      * @return True if the 'axisUom' field has been set, false otherwise.
      */
    public boolean hasAxisUom() {
      return fieldSetFlags()[5];
    }


    /**
      * Clears the value of the 'axisUom' field.
      * @return This builder.
      */
    public Energistics.Datatypes.ChannelData.ChannelAxis.Builder clearAxisUom() {
      axisUom = null;
      fieldSetFlags()[5] = false;
      return this;
    }

    @Override
    public ChannelAxis build() {
      try {
        ChannelAxis record = new ChannelAxis();
        record.axisName = fieldSetFlags()[0] ? this.axisName : (java.lang.CharSequence) defaultValue(fields()[0]);
        record.axisPropertyKind = fieldSetFlags()[1] ? this.axisPropertyKind : (java.lang.CharSequence) defaultValue(fields()[1]);
        record.axisStart = fieldSetFlags()[2] ? this.axisStart : (java.lang.Double) defaultValue(fields()[2]);
        record.axisSpacing = fieldSetFlags()[3] ? this.axisSpacing : (java.lang.Double) defaultValue(fields()[3]);
        record.axisCount = fieldSetFlags()[4] ? this.axisCount : (java.lang.Integer) defaultValue(fields()[4]);
        record.axisUom = fieldSetFlags()[5] ? this.axisUom : (java.lang.CharSequence) defaultValue(fields()[5]);
        return record;
      } catch (Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  private static final org.apache.avro.io.DatumWriter
    WRITER$ = new org.apache.avro.specific.SpecificDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  private static final org.apache.avro.io.DatumReader
    READER$ = new org.apache.avro.specific.SpecificDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
